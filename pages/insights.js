import {useEffect, useState} from 'react'
import {Tabs, Tab, Row, Col} from 'react-bootstrap'
import moment from 'moment'
import MonthlyChart from '../components/charts/BarChartIncome'
import PieChart from '../components/charts/PieChart'

export default function App(){

	const[income, setIncome] = useState([])
	const[expense, setExpense] = useState([])

	const[transactions, setTransactions] = useState([])

	useEffect(() => {
		fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}

			})
		.then(res => res.json())
		.then(data => {

			
			let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
			let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]

			data.transactions.forEach(data => {
				const index = moment(data.date).month()

				if(data.type === "income"){
					monthlyIncome[index] += data.amount
					
				} else {
					monthlyExpense[index] += data.amount
				}

			})

			setTransactions(data.transactions)
			setIncome(monthlyIncome)
			setExpense(monthlyExpense)
		})
	},[])
		
		

	return(
		<>
		<Row>
		<Col xs={12} md={5}>

		<Tabs defaultActiveKey="Income" id="monthlyFigures" className="pt-5">
			
			<Tab eventKey="Income" title="Monthly Time Spent Travelling">
				<MonthlyChart figuresArray={expense} label="Monthly total in mins" />
			</Tab>
			<Tab eventKey="spending" title="Monthly Spending">
				<MonthlyChart figuresArray={income} label="Monthly expenditure" />
			</Tab>
		</Tabs>

		</Col>

		<Col xs={12} md={7}>
		<PieChart rawData={transactions} />
		</Col>
		</Row>
		</>
	)
	
}