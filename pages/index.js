import Head from 'next/head'
import Banner from '../components/Banner'

import {Row, Col} from 'react-bootstrap'
import Register from '../components/Register'

export default function Home() {
  const data = {
    title: "Travel Tracker",
    content: "Book a ride. Record your travels. Gain insights."
  }
  return (
    <>
      <Head>
        <title>Travel Tracker</title>
      </Head>
      <Row>
        <Col xs={12} md={6}>
        <Banner data={data} />
        </Col>
        <Col xs={12} md={6}>
        <Register/>
        </Col>
      </Row>
    </>
  )
}
