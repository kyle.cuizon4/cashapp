import Head from 'next/head'
import styles from '../styles/Home.module.css'
import NavBar from '../components/Navbar'
import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Spinner } from 'react-bootstrap'
import ProfileCard from '../components/ProfileCard'
import AddTransaction from '../components/addTransaction'
import Recent from '../components/recent'
import UserContext from '../UserContext'
import Dougnut from '../components/charts/Dougnut'


export default function Home() {

	const[recent, setRecent] = useState([])
	const [userName, setUsername] = useState({})
  const [loaded, setLoaded] = useState(false)
	
	   useEffect(() => {
        fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
          
            if(data._id){
                setUsername({ name : data.firstName})
                setRecent(data.transactions)
                setLoaded(true)

            }else{
                alert('Not Found.')
            }            
        })
    }, [])
     console.log(recent)

	  const unsetUser = () => {
        localStorage.clear()
        //set the user global scope in the context provider to have its id set to null
        setUser({
            id: null
        });
    }


  return (
      <>
      		<>	
        
      			<Container>
          { loaded ? 
              <>
                <Row>
                <Col md={5}>
      				    <ProfileCard className="profile"/>
                  <AddTransaction/>
                </Col>
                 <Col md={6}>
                  
                  <Recent data={recent} />
                  <Dougnut rawData={recent}/>
                  </Col>
                </Row>
      
              </>
					   
             : 
            <div class="d-flex justify-content-center">
              <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>
            }

		      	</Container>
		    </>
       	  </>
  )
}