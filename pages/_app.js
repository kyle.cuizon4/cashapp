import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import { Container } from 'react-bootstrap'
import NavBar from '../components/Navbar'
import { UserProvider } from '../UserContext'

function MyApp({ Component, pageProps }) {

	const[recent, setRecent] = useState([])

	const [userName, setUsername] = useState({})
	const [user, setUser] = useState({
    
        id: null
    })

	   useEffect(() => {
        fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data._id){
                setUser({
                    id: data._id
                })
                setUsername({ name : data.firstName})
                setRecent(data.transactions)
            }else{
                setUser({
                    id: null
                })
            }            
        })
    }, [])

	  const unsetUser = () => {
        localStorage.clear()
        //set the user global scope in the context provider to have its id set to null
        setUser({
            id: null
        });
    }


  return(
  	<>
  
        <UserProvider value={{user, setUser, unsetUser}}>
                <NavBar />
                <Container>
                    <Component {...pageProps} />
                </Container>
        </UserProvider>
  
  	</>
  	)
   
}

export default MyApp
