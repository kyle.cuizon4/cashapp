import { useState, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import UserContext from './../UserContext';
import Router from 'next/router';
import Head from 'next/head';
import { GoogleLogin } from 'react-google-login'
import AppHelper from './../app-helper'

const login = () => {

    const { setUser } = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [tokenId, setTokenId] = useState(null)
    const [isLoading, setIsLoading] = useState(false);

  

    function authenticate(e) {
        setIsLoading(true)
        e.preventDefault();

        fetch(`https://cashappnodejs.herokuapp.com/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email, password
            })
        })
        .then( res=> res.json())
        .then( data => {
            // data
            if( data.accessToken) {
                localStorage['token'] = data.accessToken;

                fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
                    headers : {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then( res => res.json())
                .then( user =>{
                    setUser({
                        id: data._id
                    })
                     setIsLoading(false)
                    Router.push('/main')
                })
            } else {
                alert('User not registered')

            }
        })
    }

     const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`http://localhost:4000/api/users/details`, options).then(AppHelper.toJSON).then(data => {
            if( data ){
            setUser({ id: data._id, isAdmin: data.isAdmin })
            Router.push('/main')
            alert('success')
        }else {
            alert('User not registered')
        } 
        })
    }

    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <>
            <Head>
                <title>Login</title>
            </Head>
        <Card style={{ width: '30rem' , margin: 'auto' }}>
        <Card.Header>Login Details</Card.Header>
        <Card.Body>

            <Form onSubmit={e=>authenticate(e)}>
                <Form.Group controlId='userEmail'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        value={email}
                        onChange={ e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId='password'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="password"
                        value={password}
                        onChange={ e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {
                    isLoading == false ?  
                    <Button type="submit" className="w-100 text-center d-flex justify-content-center">Login</Button>
                     :  
                     <Button disabled className="w-100 text-center d-flex justify-content-center">Logging in...</Button>
                }

                <GoogleLogin
                        clientId="769488498495-bsp03vce5kp7ccdb1qpsatnoj7cl2ica.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
            
            </Form>
            </Card.Body>
    </Card>
        </>
    );
}

export default login;
