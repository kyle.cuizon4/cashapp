import React from "react";
import {Table} from 'react-bootstrap'
import styles from './styles/CountriesTable.module.css'

export default function Datatable ({data}) {

	const columns = data[0] && Object.keys(data[0])
	return (

		<Table cellPadding={0} cellSpacing={0}>
			<thead>
				<tr>{data[0] && columns.map((heading) => <th>{heading}</th>)}</tr>
			</thead>

			<tbody>
				{data.map(row => <tr>
					{
						columns.map(column => <td > {row[column]} </td>)
					}

				</tr>)}
			</tbody>
		</Table>
	)

}