
import { Row, Col, Jumbotron, Button } from 'react-bootstrap'

// import BackButton from './BackButton'
// import TravelButton from './TravelButton'
import { useRouter } from 'next/router'

export default function Banner({data}) {
    const router = useRouter()
    //destructure the data prop into its properties
    const divStyle = {
        color : 'blue',
        backgroundImage : 'url(https://unsplash.com/photos/8wMflrTLm2g)'
    }

    return (
        <Row>
            <Col xs={12} >
                <Jumbotron style={{ backgroundColor : 'transparent'}}>
                <div className="container">
                <div className="row custom-section d-flex align-items-center">
                    <div classNameww="col-12 col-lg-4">
                        <h2>CashApp</h2>
                        <h3>Process</h3>
                        <p>The only expense tracker you need!</p>
                        <a href="#">Learn more</a>
                    </div>
                    
                </div>
            </div>
                
                </Jumbotron>
            </Col>
        </Row>
    )
}