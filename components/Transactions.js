import { useEffect, useState, useContext } from 'react'
import {Table, Form, Row, Col, Container} from 'react-bootstrap';
import Datatable from "./datatable"
import moment from 'moment'
import UserContext from '../UserContext'


export default function Transactions({...rest}){
	const { user } = useContext(UserContext)
	const[data, setData] = useState([])
	const[filteredTransactions, setFilteredTransactions] = useState([])
	


	const [q, setQ] = useState("");
	const [searchColumns, setSearchColumns] = useState(["Income", "Expense"]);
	
	useEffect(() => {
		fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	if(data._id){
        		setData(data.transactions)
        		
        	}
        })
	},[])

	function search(rows){
		return rows.filter(row => 
			row.name.toLowerCase().indexOf(q) > -1 ||
			row.type.toLowerCase().indexOf(q) > -1

		)

	}

	return(
		
			<div >
        		 <form>
					<input  className="m-2" type="text" value={q} onChange={e => setQ(e.target.value)} />
					<input  className="m-2" type="radio" onClick={(e) => setQ('')} /> All
					<input  className="m-2" type="radio" value="expense" onClick={(e) => setQ(e.target.value)}/>Expense
	        		<input className="m-2" type="radio" value="income" onClick={(e) => setQ(e.target.value)} /> Income
				</form>

				<div>
					<Datatable data={search(data)} />	
				</div>
			</div>
				
		
	)
	

}

