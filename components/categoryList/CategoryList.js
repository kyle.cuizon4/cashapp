import Category from './Category'
import styles from '../styles/CountriesTable.module.css'

function categoryList({todos}){

	return(
		

		<div>
			<div>
				<ul>
					{todos.map(todo => (
					<div className={styles.row}>
						<Category key={todo.id} todo={todo} />
					</div>		
					))}
				</ul>
			</div>
		</div>

	)

}

export default categoryList