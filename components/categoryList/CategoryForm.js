import React, {useState} from 'react'
import {v4 as uuid} from "uuid";
import {Col, Button} from 'react-bootstrap'
import CheckIcon from '@material-ui/icons/Check';
import styles from '../styles/addCategory.module.css'


export default function CategoryForm({ addTodo , ...rest }){

	const [todo, setTodo] = useState({
		id:"",
		task: ""
	});

	function handleTaskInputChange(e){
		setTodo({...todo, task : e.target.value});
	}

	

	function handleSubmit(e){
		e.preventDefault();
		if(todo.task.trim()){
			addTodo({...todo, id: uuid() })
			//reset
			setTodo({...todo, task:""});
			
		}
	}

	return(
		<>
		<Col >
		<div className={styles.wrapper}>
			<input

				placeholder="Add a Category"
				name="todo"
				type="text"
				value={todo.task}
				onChange={handleTaskInputChange}
				className={styles.input}{...rest}
			 />
			<Button type="submit" onClick={handleSubmit} className="ml-2">Submit</Button>
		</div>
		</Col>
		</>
	)
}