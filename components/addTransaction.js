import {Form, Dropdown, DropdownButton, Row, Col, Card, Button, Spinner} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'




export default function transaction(){

	const { user } = useContext(UserContext)

	
	const[amount, setAmount] = useState(0);
	const[type, setType] = useState('');
	const[name, setName] = useState('');
	const[isActive, setIsActive] = useState(false);

	const[category,setCategory] = useState([])
	
	const[filteredCategory, setFilteredCategory] = useState([])

	const[local, setLocal] = useState([
		])
	const [loaded, setLoaded] = useState(true)

	
	const LOCAL_STORAGE_KEY = "react-list"


 
	
	useEffect( () => {
	
		if (typeof window !== 'undefined'){
		const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
			setLocal(storageTodos)
		}	
	},[])

	useEffect(() => {
		let tempArr =[]
		if(local){
			local.forEach(element => {
				if(!tempArr.find(category => category === element.task)){
					tempArr.push(element.task)
					setFilteredCategory(tempArr)
				}
			})
		}
	},[local])
	
	console.log(category)

	const record = (e) => {

		setLoaded(false)

		fetch(`https://cashappnodejs.herokuapp.com/api/users/transactions`, {
			method : "POST",
			headers :{
				'Authorization': `Bearer ${localStorage['token']}`,
                'Content-Type' : 'application/json'
			},

			body: JSON.stringify({
				name,
				type,
				amount,
				category
			})
		})
		.then(res => {
			if(res.ok){
				return res.json();
			} else {
				throw new Error('Something went wrong')
			}
		})
		
		.then(data => {
			if(data === true){
				setLoaded(true)
				window.location.reload()
				
			} else {
				alert('something went wrong')
			}	
		})

	}

	
	return(
		<>
		{!loaded ? 
			<div class="d-flex justify-content-center">
			  <div class="spinner-border" role="status">
			    <span class="sr-only">Loading...</span>
			  </div>
			</div>> : 
		<Card  style={{width:'20rem', marginTop:'2rem', backgroundColor: "#95bacc"}}>
		<Card.Body>
		 <Form onSubmit={e => record(e)}>
		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Description</Form.Label>
		    <Form.Control 
		    type="text-area" 
		    placeholder="Enter name of transaction" 
		    value={name}
		    onChange={e => setName(e.target.value)}
		    required />
		     <Form.Label>Amount</Form.Label>
		    <Form.Control 
		    type="number" 
		    placeholder="Enter Amount"
		    value={amount}
		    onChange={e => setAmount(e.target.value)}
		    required />
		     
		      <Form.Group controlId="formBasicCheckbox">
		      <Row className="justify-content-center mt-2">
		      	
			     <Form.Check
			          type="radio"
			          label="Expense"
			          name="formHorizontalRadios"
			          id="formHorizontalRadios1"
			          value="expense"
			          onClick={e => setType(e.target.value)}
			        />

			        <Form.Check
			        	className="ml-2"
			          type="radio"
			          label="Income"
			          name="formHorizontalRadios"
			          id="formHorizontalRadios2"
			          value="income"
			          onClick={e => setType(e.target.value)}
			        />
			     
			   </Row>	
			   <Col style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
			   <DropdownButton className="mr-4 pt-2" id="dropdown-basic-button" title="Add category">
			   		<option value="" selected disabled>Please select</option>
				  {
				  	filteredCategory.map(data => {

				  		return(
				  			<Dropdown.Item 
				  			required
				  			type="text"
				  			value={data} 
				  			onClick={(e) => setCategory(data)}> 
				  			{data} 
				  			</Dropdown.Item>
				  		)
				  	})
				  }

				</DropdownButton>
				 
				
					 <Button type="submit" className="ml-4 mt-2">Send</Button> 
				
				
				</Col>
				<p className="text-center pt-2">Category Selected : {category}</p>

			</Form.Group >
			  	
		  </Form.Group>
		  
		 </Form>
		 </Card.Body>
		 </Card>
		}
		
	
	</>
	)
}