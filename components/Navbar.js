import { useContext } from 'react'
//import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
//import nextJS Link component for client-side navigation
import Link from 'next/link'
import UserContext from '../UserContext'


export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext)

    return (
        <Navbar style={{backgroundColor: "#F2CD5D"}} expand="lg">
            <header>
                <img height={50} className="mr-3" src='https://img.icons8.com/dusk/64/000000/card-exchange.png' alt="calendar" />
            </header>
            <Link href="/main">
                <a className="navbar-brand">CashApp</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                 {(user.id !== null)
                        ? <>
                            <Link href="/category">
                                <a className="nav-link" role="button">Add a Category</a>
                            </Link>
                            <Link href="/insights">
                                <a className="nav-link" role="button">Insights</a>
                            </Link>
                            <Link href="/history">
                                <a className="nav-link" role="button">History</a>
                            </Link>
                            <Link href="/main">
                                <a className="nav-link" role="button" >Home</a>
                            </Link>
                            
                        </>
                        : <>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                        </>
                    }
                </Nav>
                        <Nav>
                        {(user.id !== null) ?
                            <Link href="/logout">
                                <a className="nav-link" role="button" >Logout</a>
                            </Link>  : ""
                        }    
                        </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}