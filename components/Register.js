import { useState, useEffect, useContext } from 'react'
import { Form, Button, Card, Spinner } from 'react-bootstrap'
import Router from 'next/router'
import Head from 'next/head'


export default function register() {
     
    //form input state hooks
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [name, setName] = useState('')
    const [lastName, setLastName] = useState('')

    //state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)
    const [loading, setLoading] = useState(false)

    //validate form input whenever email, password1, or password2 is changed
    useEffect(() => {
        //validation to enable submit button when all fields are populated and both passwords match
        if((password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [password1, password2])



    //function to register user
    function registerUser(e) {
        setLoading(true)
        e.preventDefault();

        //check for duplicate email in database first
        fetch(`https://cashappnodejs.herokuapp.com/api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no duplicates found
            if (data === false){
                fetch(`https://cashappnodejs.herokuapp.com/api/users`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                    	firstName : name,
                    	lastName : lastName,
                        email: email,
                        password: password1,
                        loginType : "Normal"
                    })
                })
                .then(res => res.json())
                .then(data => {
                    setLoading(false)
                    //registration successful
                    if(data === true){
                        //redirect to login
                        Router.push('/login')
                    }else{
                        //error in creating registration, redirect to error page
                        Router.push('/error')
                    }
                })
            }else{//duplicate email found
                Router.push('/error')
            }
        })
    } 

    return (
        <>
        { loading ?  

           <div class="d-flex justify-content-center">
              <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>

           : <Head>
                <title>Register</title>
            </Head>
            <Card style={{ marginTop : '2rem'}}>
            <Card.Body>
            <Form onSubmit={(e) => registerUser(e)}>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="name" placeholder="Enter name" value={name} onChange={e => setName(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="name" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
                </Form.Group>


                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                </Form.Group>

                {/* conditionally render submit button based on isActive state */}
                {isActive
                    ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                }
                
            </Form>
            </Card.Body>

            </Card>
        </>}
    )
}