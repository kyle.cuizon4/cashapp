import {Table, Row, Col } from 'react-bootstrap'
import {useEffect, useState, useContext} from 'react'
import moment from 'moment'
import UserContext from '../UserContext'
import styles from './styles/CountriesTable.module.css'

export default function recent({data}){
	const { user } = useContext(UserContext)
	const[recentTransaction, setRecentTransaction] = useState([])

	useEffect( () => {
		setRecentTransaction(data)
	
	})

	return(
		<Table>
			<thead>
				<tr className={styles.heading}>
					<th className={styles.name}>Name</th>
					<th className={styles.name}>Type</th>
					<th className={styles.name}>Amount</th>
					<th className={styles.name}>Date</th>
					<th className={styles.name}>Category</th>
				</tr>
			</thead>
			<tbody className={styles.body}>
					{
						recentTransaction.map(record => {
							return(
							
								<tr key={record._id} className={styles.row}>
									<td className={styles.name}>{record.name}</td>
									{
										record.type === "income" ? <td style={{color: "green"}} className={styles.name}>{record.type}</td> : <td style={{color: "red"}} className={styles.name}>{record.type}</td>
									}
									<td className={styles.name}> &#8369; {record.amount}</td>


									<td className={styles.name}>{moment(record.date).format("MMM Do YY")}</td>
									<td className={styles.name}>{record.category}</td>
								</tr>
							
							)	
						})
					}

			</tbody>
		</Table>

	)
}