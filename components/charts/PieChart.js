import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../../helper';


const SalesDistributionByBrand = ({rawData}) => {

	

	const[filter, setFilter] = useState([])
	
	const[categories, setCategories] = useState([])
	const[amount, setAmount] = useState([])
	const[bgColors, setBgColors] = useState([])
	
	useEffect(() => {
		let cats = []
		rawData.forEach( element => {
			if(!cats.find(type => type === element.category)){
				cats.push(element.category)
			}
		})
		setCategories(cats)
	},[rawData])

	

	useEffect(() => {
		setAmount(categories.map(category => {
			let tempAmount = 0;
			rawData.forEach(element => {

				if (element.category === category){
					tempAmount = tempAmount + parseInt(element.amount)
				}
			})
			return tempAmount;
			console.log(amount)

		}))
		setBgColors(categories.map(() => `#${colorRandomizer()}`))
	},[categories])


	const data = {

		labels : categories,
		datasets : [{
			data : amount,
			backgroundColor : bgColors,
			hoverBackgroundColor : bgColors
		}]
	}


	return(

		<>	
			<div className="pt-5">
			<Pie data={data} />
			</div>
		</>

	)
}

export default SalesDistributionByBrand;