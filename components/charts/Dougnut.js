import { useState, useEffect } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { colorRandomizer } from '../../helper';

const Doughnuts = ({rawData}) => {

const[type, setType] = useState([])

const[amount, setAmount] = useState([])


 useEffect(() => {
 	let tempType = []
 	 	 rawData.forEach(element => {
 	 		  if(!tempType.find(type => type === element.type)){
 	 			   tempType.push(element.type)
 	 		   }
 	 		})
 	 	setType(tempType)

 	 },[rawData])

  useEffect(() => {
		setAmount(type.map( element => {
			let tempAmount = 0;
			rawData.forEach( data => {

			 	if(data.type === element){
			    	tempAmount = tempAmount + parseInt(data.amount)
			  		}
	 			})
			  	return tempAmount;
			  	console.log(amount)
			}))
	 },[type])

   
	const data = {

		labels : type,
		datasets : [{
			data : amount,
			backgroundColor : ["#00d977", "#630b11"]
		}]
	}

	return(
		<>
			<Doughnut data={data} />
		</>
	)
}

export default Doughnuts


