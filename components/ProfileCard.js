import {Card, Button, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import styles from './styles/profile.module.css'

export default function profile(){

    const { user } = useContext(UserContext)

	const[records, setRecords] = useState([])
	const[name, setName] = useState([])
	const[lastName, setLastName] = useState([])
	const[email, setEmail] = useState([])
 
	const [total, setTotal] = useState([])
	const[incomeAmounts, setIncomeAmounts] = useState([])
	const[expenseAmounts, setExpenseAmounts] = useState([])


	 useEffect(() => {
        fetch(`https://cashappnodejs.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data._id){//JWT validated
                setRecords(data.transactions)
                setName(data.firstName)
                setLastName(data.lastName)
                setEmail(data.email)

                let tempIncomeAmounts = []
                let tempExpenseAmounts = []

                data.transactions.forEach(element => {
                	if(element.type === "income"){
                		tempIncomeAmounts.push(element.amount)
                		setIncomeAmounts( tempIncomeAmounts.reduce((acc,item) => (acc += item),0))
                		

                	} else if (element.type === "expense"){
                		tempExpenseAmounts.push(element.amount)
                		setExpenseAmounts( tempExpenseAmounts.reduce((acc,item) => (acc += item),0))
                		

                	}
                })
       
            }else{
                setRecords([])
            }            
        })
    }, [])

	 useEffect(() => {

	 		setTotal(incomeAmounts - expenseAmounts)
	 		console.log(total)
	 },[incomeAmounts, expenseAmounts])

	   
	return(

		<Card style={{ width: '18rem', marginBottom: '2rem' , backgroundColor: "#95bacc", marginTop:'2rem'}} >
			 
			  <Card.Body>
			    <Card.Title className="text-center mb-4 pt-100"><h2>{name} {lastName}</h2></Card.Title>
			    <Card.Title className="text-center" style={{fontSize: "25px" , color:"green"}}>Balance: &#8369; {total}</Card.Title>
			    <Card.Text className="text-center">
			      {email}
			    </Card.Text>
                <div className="text-center">
			    <Button  variant="success">View Insights</Button>
                </div>
			  </Card.Body>
		</Card>
	)
}