import {Row, Col} from 'react-bootstrap'
import {useEffect, useState, useContext} from 'react';

import CategoryForm from './categoryList/CategoryForm'
import CategoryList from './categoryList/CategoryList'
import UserContext from '../UserContext'

import styles from './styles/addCategory.module.css'

export default function Category({...rest}){

	const { user } = useContext(UserContext)
	
	const [todos, setTodos] = useState([]);

	const LOCAL_STORAGE_KEY = "react-list"

	useEffect(() => {
		const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))

		if(storageTodos){
			setTodos(storageTodos)
		}
	},[])

	useEffect(() => {
		localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
	},[todos])



	function addTodo(todo){
		setTodos([todo, ...todos]);
	}	

	return(
		<>
		
			<div className="pt-5">
				<header>
					<h3 className="text-center">Add a Category</h3>
					<Row>
					 <CategoryForm addTodo={addTodo} color="inherit" />
					</Row>
					<CategoryList todos={todos} />

				</header>
			</div>
		

		</>
	)
}